%global commit 7e15f28f6c76b06d26fb5d8525f569e54c3784fc
%global shortcommit %(c=%{commit}; echo ${c:0:7})

%global         checkout 20130124git%{shortcommit}

Name:           capillary-js
Version:        0.1.0
Release:        1.%{checkout}%{?dist}
Summary:        Git graphs with Javascript
Group:          Applications/Internet
License:        AGPLv3+
URL:            https://gitorious.org/capillary/capillary-js
# To download the source tarball:
# curl -o capillary-js-%{shortcommit}.tar.gz https://gitorious.org/capillary/capillary-js/archive-tarball/%{commit}
# You may get a "try again later" message at first.
Source0:        %{name}-%{shortcommit}.tar.gz
Source1:        %{name}.conf
BuildArch:      noarch

#BuildRequires:  
Requires:       httpd

%description
Capillary parses the output of Git log and produces a beautiful
graphical representation of your repository's history - similar to gitk
and git log --graph, except beautiful. capillary.js is the Javascript
library doing the presentation.


%prep
%setup -q -n capillary-%{name}

%build

%install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}
cp -p -r lib  $RPM_BUILD_ROOT%{_datadir}/%{name}
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d
install -p -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d

%files
%doc docs
%{_datadir}/%{name}
%config(noreplace) %{_sysconfdir}/httpd/conf.d/%{name}.conf

%changelog
* Fri Jan 24 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.1.0-1.20130124git7e15f28f
- Initial packaging
